#pragma once

#include <ostream>

namespace std
{
    extern ostream cout;
    extern ostream cerr;
    extern ostream clog;
}
