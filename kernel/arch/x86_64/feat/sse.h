#pragma once

namespace x86_64
{
    bool sse_check();

    void sse_enable();
}
