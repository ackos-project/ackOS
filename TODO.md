# ackOS overall progress
A more detailed to do list is available on the projects tab of GitHub

## Step #1
* [x] Make use of x86 functionality
* [x] Memory Management
* [x] Basic Filesystem
* [ ] ACPI/APIC
* [ ] SMP
* [x] System calls
* [x] Libraries (libc, libstdcpp)

## Step #2
* [ ] Userspace
* [ ] Software (e.g shell, coreutils)
* [ ] GUI
* [ ] Devices (sound, USB, network)
* [ ] and beyond that...
